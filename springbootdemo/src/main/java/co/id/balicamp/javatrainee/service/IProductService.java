package co.id.balicamp.javatrainee.service;

import co.id.balicamp.javatrainee.model.Product;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 11:36
 */
public interface IProductService {
    List<Product> getAllProducts();

    Product getProductById(String id);

    void addDataProduct(Product product);

    void updateDataProduct(Product product, String id);

    void deleteProductById(String id);
}
