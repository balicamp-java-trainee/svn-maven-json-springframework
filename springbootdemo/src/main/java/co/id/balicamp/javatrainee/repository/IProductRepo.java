package co.id.balicamp.javatrainee.repository;

import co.id.balicamp.javatrainee.model.Product;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 11:48
 */
public interface IProductRepo {
    List<Product> getAll();

    Product getById(String id);

    void save(Product product);

    void update(Product product, String id);

    void deleteById(String id);
}
