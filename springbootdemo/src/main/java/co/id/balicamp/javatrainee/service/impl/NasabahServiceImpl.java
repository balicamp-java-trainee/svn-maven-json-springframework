package co.id.balicamp.javatrainee.service.impl;

import co.id.balicamp.javatrainee.model.Nasabah;
import co.id.balicamp.javatrainee.repository.INasabahDao;
import co.id.balicamp.javatrainee.service.INasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:15
 */
@Service
public class NasabahServiceImpl implements INasabahService {

    @Autowired
    private INasabahDao nasabahDao;

    @Override
    public List<Nasabah> getAllNasabah() {
        return nasabahDao.getAll();
    }

    @Override
    public Nasabah getNasabahById(String id) {
        return nasabahDao.getById(id);
    }

    @Override
    public String addNasabah(Nasabah nasabah) {
        try {
            nasabahDao.add(nasabah);
            return "SUCCESS";
        }catch (Exception e){
            return "ERROR";
        }
    }

    @Override
    public String editNasabah(Nasabah nasabah) {
        try {
            nasabahDao.edit(nasabah);
            return "SUCCESS";
        }catch (Exception e){
            return "ERROR";
        }
    }

    @Override
    public List<Nasabah> getNasabahBySidActive() {
        return nasabahDao.getBySidActive();
    }

    @Override
    public Nasabah deleteNasabahSidById(String id) {
        return nasabahDao.deleteSidByNasabahId(id);
    }
}
