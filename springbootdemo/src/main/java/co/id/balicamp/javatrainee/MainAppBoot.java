package co.id.balicamp.javatrainee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 09:46
 */
@SpringBootApplication
public class MainAppBoot {

    public static void main(String[] args) {
        SpringApplication.run(MainAppBoot.class, args);
    }
}
