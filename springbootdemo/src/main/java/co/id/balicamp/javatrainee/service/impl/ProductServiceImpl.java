package co.id.balicamp.javatrainee.service.impl;

import co.id.balicamp.javatrainee.model.Product;
import co.id.balicamp.javatrainee.repository.IProductRepo;
import co.id.balicamp.javatrainee.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 11:29
 */
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private IProductRepo productRepo;

    @Override
    public List<Product> getAllProducts() {
        return productRepo.getAll();
    }

    @Override
    public Product getProductById(String id) {
        return productRepo.getById(id);
    }

    @Override
    public void addDataProduct(Product product) {
        productRepo.save(product);
    }

    @Override
    public void updateDataProduct(Product product, String id) {
        productRepo.update(product, id);
    }

    @Override
    public void deleteProductById(String id) {
        productRepo.deleteById(id);
    }


}
