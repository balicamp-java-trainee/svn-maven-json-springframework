package co.id.balicamp.javatrainee.repository.impl;

import co.id.balicamp.javatrainee.model.Product;
import co.id.balicamp.javatrainee.repository.IProductRepo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 11:45
 */
@Repository
public class ProductRepoImpl implements IProductRepo {

    private List<Product> products = new ArrayList<>(Arrays.asList(
            new Product("P001", "Product Name P001", "Description Product P001"),
            new Product("P002", "Product Name P002", "Description Product P002"),
            new Product("P003", "Product Name P003", "Description Product P003"),
            new Product("P004", "Product Name P004", "Description Product P004")
    ));


    @Override
    public List<Product> getAll() {
        return products;
    }

    @Override
    public Product getById(String id) {
        return products.stream().filter(product -> product.getId().equalsIgnoreCase(id)).findFirst().get();
    }

    @Override
    public void save(Product product) {
        products.add(product);
    }

    @Override
    public void update(Product product, String id) {
        for (int i = 0; i < products.size(); i++) {
            Product p = products.get(i);
            if(p.getId().equalsIgnoreCase(id)){
                products.set(i, product);
                return;
            }
        }
    }

    @Override
    public void deleteById(String id) {
        products.removeIf(product -> product.getId().equalsIgnoreCase(id));
    }
}
