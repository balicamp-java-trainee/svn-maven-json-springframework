package co.id.balicamp.javatrainee.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sid {
    private String id;
    private Integer sidNo;
    private Boolean isActive = true;
    private String idNasabah;
}
