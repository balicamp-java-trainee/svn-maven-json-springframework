package co.id.balicamp.javatrainee.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Nasabah {
    private String id;
    private String name;
    private String address;
    private List<Sid> sid;
}
