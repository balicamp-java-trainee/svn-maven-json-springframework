package co.id.balicamp.javatrainee.controller;

import co.id.balicamp.javatrainee.model.Product;
import co.id.balicamp.javatrainee.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 10:46
 */
@RestController
public class ProductController {

    @Autowired
    private IProductService productService;

    @RequestMapping("/")
    public String hello(){
        return "Hello Spring Boot";
    }

    @RequestMapping("/view-products")
    public List<Product> getProducts(){
        return productService.getAllProducts();
    }

    @RequestMapping("/view-products/{idProduct}")
    public Product getProductById(@PathVariable("idProduct") String id){
        return productService.getProductById(id);
    }

    @RequestMapping(value = "/transaction-add-product", method = RequestMethod.POST)
    public void addProduct(@RequestBody Product product){
        productService.addDataProduct(product);
    }

    @RequestMapping(value = "/transaction-update-product/{id}", method = RequestMethod.PUT)
    public void updateProduct(@RequestBody Product product, @PathVariable String id){
        productService.updateDataProduct(product, id);
    }

    @RequestMapping(value = "/transaction-delete-product/{id}", method = RequestMethod.DELETE)
    public void deleteProduct(@PathVariable String id){
        productService.deleteProductById(id);
    }
}
