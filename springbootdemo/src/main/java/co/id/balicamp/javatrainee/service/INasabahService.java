package co.id.balicamp.javatrainee.service;

import co.id.balicamp.javatrainee.model.Nasabah;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:15
 */
public interface INasabahService {
    List<Nasabah> getAllNasabah();

    Nasabah getNasabahById(String id);

    String addNasabah(Nasabah nasabah);

    String editNasabah(Nasabah nasabah);

    List<Nasabah> getNasabahBySidActive();

    Nasabah deleteNasabahSidById(String id);
}
