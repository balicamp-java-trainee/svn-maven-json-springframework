package co.id.balicamp.javatrainee.controller;

import co.id.balicamp.javatrainee.model.Nasabah;
import co.id.balicamp.javatrainee.model.TrxIdDto;
import co.id.balicamp.javatrainee.service.INasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:13
 */
@RestController
public class NasabahController {

    @Autowired
    private INasabahService nasabahService;

    @RequestMapping(value = "/view-datanasabah", method = RequestMethod.POST)
    public List<Nasabah> getAllNasabah(){
        return nasabahService.getAllNasabah();
    }

    @RequestMapping(value = "/view-datanasabah-by-id", method = RequestMethod.POST)
    public Nasabah getNasabahById(@RequestBody TrxIdDto id){
        return nasabahService.getNasabahById(id.getId());
    }

    @RequestMapping(value = "/transaction-add-nasabah", method = RequestMethod.POST)
    public String addDataNasabah(@RequestBody Nasabah nasabah){
        return nasabahService.addNasabah(nasabah);
    }

    @RequestMapping(value = "/transaction-update-nasabah", method = RequestMethod.POST)
    public String editDataNasabah(@RequestBody Nasabah nasabah){
        return nasabahService.editNasabah(nasabah);
    }

    @RequestMapping(value = "/view-get-nasabah-by-sid-active", method = RequestMethod.POST)
    public List<Nasabah> getNasabahBySidActive(){
        return nasabahService.getNasabahBySidActive();
    }

    @RequestMapping("/transaction-delete-sid-by-nasabah-id")
    public Nasabah deleteSidNasabahById(@RequestBody TrxIdDto id){
        return nasabahService.deleteNasabahSidById(id.getId());
    }
}
