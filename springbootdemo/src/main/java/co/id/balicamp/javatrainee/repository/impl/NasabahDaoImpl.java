package co.id.balicamp.javatrainee.repository.impl;

import co.id.balicamp.javatrainee.model.Nasabah;
import co.id.balicamp.javatrainee.model.Sid;
import co.id.balicamp.javatrainee.repository.INasabahDao;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:17
 */
@Repository
public class NasabahDaoImpl implements INasabahDao {

    private List<Nasabah> nasabahs = new ArrayList<>(Arrays.asList(
            new Nasabah("N001", "Irwan Santoso", "Jl. Serma Tugir Denpasar",
                    Arrays.asList(
                            new Sid("S0101", 1234567891, true, "N001"),
                            new Sid("S0102", 1234567890, false, "N001"),
                            new Sid("S0103", 1223456789, false, "N001")
                            )),
            new Nasabah("N002", "Budi Gunawan", "Jakarta Selatan, Menteng",
                    Arrays.asList(
                            new Sid("B0101", 1234567891, true, "N002"),
                            new Sid("B0102", 1234567890, false, "N002"),
                            new Sid("B0103", 1223456789, true, "N002")
                    )),
            new Nasabah("N003", "Rudi Sadria", "Wisma BCA BSD Tanggerang",
                    Arrays.asList(
                            new Sid("R0101", 1234567891, true, "N003"),
                            new Sid("R0102", 1234567890, true, "N003"),
                            new Sid("R0103", 1223456789, true, "N003")
                    )),
            new Nasabah("N004", "Ridla Fadilah", "Kuningan Jakarta Selatan",
                    Arrays.asList(
                            new Sid("F0101", 1234567891, false, "N004"),
                            new Sid("F0102", 1234567890, false, "N004"),
                            new Sid("F0103", 1223456789, true, "N004")
                    )),
            new Nasabah("N005", "Ferdiansyah", "Bandung, PVJ",
                    Arrays.asList(
                            new Sid("D0101", 1234567891, true, "N005"),
                            new Sid("D0102", 1234567890, true, "N005"),
                            new Sid("D0103", 1223456789, true, "N005")
                    ))
    ));


    @Override
    public List<Nasabah> getAll() {
        return nasabahs;
    }

    @Override
    public Nasabah getById(String id) {
        return nasabahs.stream().filter(nasabah -> nasabah.getId().equalsIgnoreCase(id)).findFirst().get();
    }

    @Override
    public void add(Nasabah nasabah) {
        nasabahs.add(nasabah);
    }

    @Override
    public void edit(Nasabah nasabah) {
        for (int i = 0; i < nasabahs.size(); i++) {
            Nasabah nasabah1 = nasabahs.get(i);
            if(nasabah1.getId().equalsIgnoreCase(nasabah.getId())){
                nasabahs.remove(i);
                nasabahs.add(i, nasabah);
                return;
            }
        }
    }

    @Override
    public List<Nasabah> getBySidActive() {
        List<Nasabah> response = new ArrayList<>();
        nasabahs.forEach(nasabah -> {
            Nasabah nb = new Nasabah(nasabah.getId(), nasabah.getName(), nasabah.getAddress(),
                            nasabah.getSid()
                            .stream()
                            .filter(sid -> sid.getIsActive().equals(Boolean.TRUE))
                            .collect(Collectors.toList())
                    );
            response.add(nb);
        });
        return response;
    }

    @Override
    public Nasabah deleteSidByNasabahId(String id) {
        for (int i = 0; i < nasabahs.size(); i++) {
            Nasabah nb = nasabahs.get(i);
            if(nb.getId().equalsIgnoreCase(id)){
                nb.getSid().forEach(sid -> sid.setIsActive(Boolean.FALSE));
                return nb;
            }
        }
        return null;
    }
}
