package co.id.balicamp.javatrainee.model;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 17:09
 */
@Data
public class TrxIdDto {
    private String id;
}
