package co.id.balicamp.javatrainee.repository;

import co.id.balicamp.javatrainee.model.Nasabah;

import java.util.List;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: $
 * Created by IntelliJ IDEA.
 * Date: 2019-02-27
 * Time: 16:16
 */
public interface INasabahDao {
    List<Nasabah> getAll();

    Nasabah getById(String id);

    void add(Nasabah nasabah);

    void edit(Nasabah nasabah);

    List<Nasabah> getBySidActive();

    Nasabah deleteSidByNasabahId(String id);
}
