# Roadmap #

* Overview
* Layer Application Design Pattern
* Spring Boot Family Introduction
* Spring Rest

## Spring Framework ##

**Latar belakang :** Dalam skala Enterprise Application dikenal adanya konsep MVC (Modelling View Controller) yang mana konsep ini akan memisahkan proses pembangunan aplikasi kepada beberapa layer utama. Berawal dari konsep "**_separation of concern_**", dimana tiap layer memiliki fungsi yang berbeda-beda sehingga perlu dipisahkan.

**Point :**
- Spring merupakan **application framework** Java yang mempermudah dalam proses development aplikasi Enterprise berbasis Java
    Salah satu design-pattern andalannya adalah _dependency-injection_ (_DI_).
    Beberapa fitur yang disediakan oleh Spring Framework adalah: 
    1. Dependency Injection (tidak dibahas pada artikel ini)
    2. Aspect Oriented Programming (tidak dibahas pada artikel ini)
    3. Spring MVC dan Restful Web Service
    4. Support koneksi database, dsb
- Konsepnya adalah **programming and configuration model** dapat meyediakan lib/configuration bisnis yang akan kita kembangkan, serahkan semuanya pada spring 😀
- **Infrastructure support** karena spring mensupport konektivitas ke database, baik itu RDBMS seperti MySQL, PostgreeSQL, ataupun mongoDB dll

Spring secara bertahap melakukan integrasi teknologi, dan mengelola seluruh bisnis objek pada satu tempat saja kedalam satu file konfigurasi. Contohnya penggunaan JDBC untuk koneksi database,
Time Scheduler, JMS (_Java Messaging Service_), DAO (_Data Access Object_) dll. Dari keunikan diatas spring menjadi framework yang bukan hanya framework biasa, ia memiliki fokus pada pengelolaan bisnis objek yang sangat berguna pada aplikasi-aplikasi enterprise.

**Permasalahan :**
- Spring adalah framework yang sangat amat besar (**huge framework**), karena tujuan awalnya adalah dapat mencover/mensupport semua keberagaman dan perbedaan kebutuhan pengelolaan bisnis aplikasi-aplikasi enterprise (older years lah pokoknya 😁 ). Perumpamaan,
suatu aplikasi enterprise, membutuhkan sesuatu yang mengandung berbagai kombinasi dan bermacam-macam konfigurasi, sehingga muncul jawaban --> gunakanlah spring framework, karena ia dapat memberikannya!
- Membutuhkan **multiple setup steps**, karena semakin beragam maupun semakin besar kemampuan aplikasi yang akan kita kembangkan membutuhkan banyak waktu dan pertimbangan yang diperlukan untuk membuatnya dari awal.
- Membutuhkan **multiple configuration steps**, selain step yang beragam maka akan diikuti pula dengan banyak configurasi yang spesifik berdasarkan bisnis yang dikembangkan perlu dipersiapkan.
- Membutuhkan **multiple build & deploy steps**, tentu memunculkan banyak cara dan berbagai kombinasi dalam build serta deploynya.

> Key : Dengan beragam kemampuan dan fleksibilitas yang disediakan spring yang "_super_" tentu datang dengan **cost**, dan kita selalu menganggap bahwa itulah best practice dalam membangun suatu enterprise aplikasi

**Pertanyaan yang muncul :**
> **Can we abstract these step?**

**Kesimpulannya** adalah kita membutuhkan suatu jenis framework yang spesifik mencerminkan bisnis logic dan spesifik fungsinya dapat menghandle proses bisnis dalam aplikasi yang akan dikembangkan sesuai kebutuhan saja, sehingga dapat mengurangi cost.

## Spring Boot ##

**Spring** adalah spring framework + **Boot** adalah bootstrap 


> **Spring Boot** makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

**Spring Boot** merupakan salah satu jenis framework (Bundling) mengintegrasikan berbagai library dari Spring dan Spring family yang telah dilengkapi dengan beberapa fitur:
* Telah disediakan (Embed) server tomcat, jetty dan server lain sehingga kita hanya perlu _"run"_
* Telah disediakan build manager seperti maven yang dapat diatur di _Project Object Model_ (POM)
* Pattern andalan (_DI_) mengakomudasi penulisan kode secara _clean_ dan _loosely coupled_ (suatu kelas tidak terikat erat dengan kelas yang lain sehingga hubungan antar bagian kode menjadi longgar), yang memanfaatkan _annotation_ untuk mempermudah dalam menentukan komponen maupun class, dsb
* Auto Configuration

#### Spring Boot Starters ####

Dependency management sangat krusial dalam project, baik itu yang cukup kompleks, _memanage_ nya akan membutuhkan waktu dan tenaga. Maka dari itu Spring Boot Starter akan sangat berguna. 
Semua starters dibawah _org.springframework.boot_ dan berjumlah lebih dari 50 starters. Namun yang paling banyak digunakan diantaranya:
* _spring-boot-starter_ : core starter, including auto-configuration support, logging, and YAML
* _spring-boot-starter-aop_ : starter for aspect-oriented programming with Spring AOP and AspectJ
* _spring-boot-starter-data-jpa_ : starter for using Spring Data JPA with Hibernate
* _spring-boot-starter-jdbc_ : starter for using JDBC with the HikariCP connection pool
* _spring-boot-starter-security_ : starter for using Spring Security
* _spring-boot-starter-test_ : starter for testing Spring Boot applications
* _spring-boot-starter-web_ : starter for building web, including RESTful, applications using Spring MVC

#### Development Environment ####

- **Setup: Java 8 SDK**
- **IDE: STS (_Spring Tool Suite_), Eclipse, Idea, etc**
- **Maven is a MUST**


#### Hello, Spring Boot! ####
Berikut adalah bagaimana membuat sebuah project sederhana berbasis spring boot. Untuk membuat aplikasi java berbasis spring boot, ada berbagai macam cara yang dapat ditempuh.

##### Maven Project #####
Langkah pertama adalah dengan membuat sebuah _maven project_ melalui IDE yang digunakan (dalam artikel ini digunakan Intellij Idea Community Edition).

1. Buka Intellij dan Create New Project ![maven-project_1](img/maven_project_1.png)

2. Pada pilihan project, pilih maven ![maven-project_2](img/maven_project_2.png)

3. Lengkapi Identifier project sesuai keperluan ![maven-project_3](img/maven_project_3.png)

4. Cek finalisasi sumary project name dan yang lainnya, kemudian finish ![maven-project_4](img/maven_project_4.png)

5. Setelah terbentuk skeleton projectnya, maka struktur folder projectnya akan seperti gambar berikut ![maven-project_4](img/maven_project_5.png)
    * `src/main/java` : Tempat source code utama
    * `src/main/resources` : Tempat ditempatkannya file configurasi, file static berupa image, icon, dan lain-lain
    * `target` : folder hasil compile maven (setelah menjalankan lifecycle `mvn clean install`)

6. Langkah berikutnya adalah melengkapi `pom.xml`

    ```java
    <!-- Code diatasnya tidak ditampilkan -->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.10.RELEASE</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
    ```

7. Menambahkan class main pada `src/main/java/co/id/balicamp/javatrainee`

    ```java
    @SpringBootApplication
    public class MavenSpringBootDemo {

        public static void main(String[] args) {
            SpringApplication.run(MavenSpringBootDemo.class, args);
        }
    }
    ```

8. Tambahkan Run Configuration sebagai java application 
    
    ![maven-project_5](img/maven_project_6.png)
    
    atau dapat langsung dijalankan pada class main 
    ![maven-project_5](img/maven_project_7.png)
    
9. Akan muncul pada console run application bahwa MavenSpringBootDemo telah _start_ dan _listen_ pada port 8080
![maven-project_5](img/maven_project_8.png)

10. Coba akses via browser local ke halaman `http://localhost:8080/`
![localhost](img/localhost.png)


##### Starting Spring Boot #####

```java
SpringApplication.run(App.class, args);
```

Apakah yang terjadi saat kita run aplikasi spring boot dari class main diatas? beberapa steps yang terjadi adalah
* Sets up default configuration
* Starts Spring application context
* Perform class path scan
* Starts tomcat server

## Design Pattern: Controller, Service, DAO ##

Konsep MVC (Modelling View Controller) dalam penerapannya, layer bisa dipisahkan menjadi 3 bagian utama 

1. Presentation Layer
2. Business Layer
3. Data Access Layer

(+) Keuntungannya :
- Pemecahan class yang mereprentasikan data (Model), tampilan (View) dan pengaturan pertukaran data (Controller)
- Distribusi development yang dapat disebar antara tim development secara paralel
- Mudah dalam maintenance jangka panjang

(-) Kekurangan :
- Lebih merepotkan diawal, karena harus menyiapkan/membuat lebih banyak class dan package sesuai konsep MVC

Struktural pattern dalam java project bertujuan untuk memilah/meng-isolate aplikasi/bisnis layer dengan _persitance layer_ (seperti misalnya yang berkaitan dengan database). Struktural layer yang umum digunakan dalam membangun sebuah java project:

![mvc](img/mvc.png)

- **View** merupakan kumpulan class yang merepresentasikan user interface
- **Controller**  merupakan class yang mengendalikan alur program secara keseluruhan, secara teknis mengandung business-logic sebagai penghubung antara layer view dengan dao, model atau service yang diinginkan
- **Model** merupakan class yang merepresentasikan struktur data suatu konsep object maupun database. Dalam konsep java biasanya disebut POJO (_Plain Old Java Object_) lengkap dengan berbagai property, method getter-setter, dsb
- **DAO (_Data Access Object_)** merupakan kumpulan class yang melakukan manipulasi data (CRUD: Create, Read, Update, Delete) pada database
- **Database** merupakan representasi class untuk mengatur konfigurasi koneksi ke database/data storage tertentu
Pendekatan lain biasanya ditambahkan layer satu lagi, yaitu
- **Service** merupakan layer yang menjadi penghubung antara business-logic pada controller dengan layer persistance (DAO atau Database)

## RESTful Web Service ##

**RESTful** merupakan salah satu dari jenis web service, dengan menggunakan protokol HTTP (_Hypertext Transfer Protocol_) dalam pertukaran data antar state. Format pertukaran data diantaranya : **_JSON, XML_** dll.

> **REST** is a _late-binding_ dalam artian menggunakan variabel generic tanpa tipe data layaknya Javascript. 
Suitable actions (**GET, POST, PUT, DELETE**)

- POST - Create a new resource
- GET - Read a resource
- PUT - Update an existing resource
- DELETE - Delete a resource

#### Getting Started! ####

Pada penjabaran berikut, kita menggunakan single resource (static/hardcode) untuk mendalami konsep RESTful Web Service tanpa bersinggungan dengan database layer (akan dibahas pada Spring JPA).
Pertama kita akan mencoba membuat sebuah class controller sederhana. 

Beberapa point penting saat menambahkan controller pada aplikasi web berbasis spring boot, diantaranya:
* Mulai masuk pada server-side code
* Penambahan berupa Java Class
* Dilengkapi dengan annotations `@RestController`
* Mengandung informasi tentang :
    - URL http yang dapat diakses sebagai triggernya
    - Method apa yang dikerjakan saat urlnya diakses

Tambahkan Class controller `ProductController` pada package `co.id.balicamp.javatrainee.api`, Class terdiri dari potongan code berikut:
```java
package co.id.balicamp.javatrainee.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @RequestMapping("/")
    public String sayHiProduct(){
        return "Hi Product...";
    }
}
```

Perhatikan potongan code diatas, terdapat beberapa _annotation_ yang ditambahkan diantaranya `@RestController` dan `@RequestMapping`. Kedua _annotation_ tersebut merupakan bagian dari configuration yang kita inject untuk memberi petunjuk kepada spring boot bahwa,
- `@RestController` menandakan class ProductController merupakan sebuah rest controller
- `@RequestMapping` menandakan mapping url path http yang akan diekspose sebagai path yaitu `/` dengan method sayHiProduct

Berikutnya kita akan mencoba membuat sebuah model class POJO `Product` sebagai media pertukaran informasi antara business layer dengan presentation layer. Dalam model `Product` digunakan 
library `Lombok` untuk mempermudah dalam pembuatan class POJO sehingga tidak perlu memenuhi code dengan getter/setter dsb. Pertama kita harus menambahkan dependency pada `pom.xml`
```java
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
</dependency>
```

Dan berikut adalah potongan code Class Product pada package `co.id.balicamp.javatrainee.model` 
```java
    package co.id.balicamp.javatrainee.model;

    import lombok.AllArgsConstructor;
    import lombok.Data;
    import lombok.NoArgsConstructor;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Product {
        private String id;
        private String name;
        private String description;
    }
```

Pada potongan class Product, terdapat beberapa _annotation_, 
- `@Data` adalah _annotation_ pengganti getter & setter untuk masing-masing property class
- `@NoArgsConstructor` adalah pengganti constructor default (tanpa argument) pada class
- `@AllArgsConstructor` adalah pengganti constructor class dengan argument seluruh property yang terdapat pada class


Setelah menyiapkan class POJO, selanjutnya kita akan menyiapkan controller yang me-_return_ Object sebagai response, dalam hal ini menggunakan class `Product` sebagai return method.
```java
    // Potongan code lainnya tidak ditampilkan 
    @RequestMapping("/products")
    public List<Product> getAllProducts(){
        return Arrays.asList(new Product("P001", "Product 001", "Product Description 001"),
                new Product("P002", "Product 002", "Product Description 002"),
                new Product("P003", "Product 003", "Product Description 003"),
                new Product("P004", "Product 004", "Product Description 004")
            );
    }
```

Dengan penambahan code di atas, dapat dicoba kembali untuk mengakses url path `localhost:8080/products` untuk mendapatkan response berupa data product.

**Melangkah ke level business service**

Dalam konsep MVC, beberapa class diatas telah dapat mengakomudasi kebutuhan general. Namun apa yang terjadi ketika controller lain memerlukan data product juga? Maka dibutuhkan untuk me-_recreate_ ulang potongan code yang menghasilkan kumpulan data product, selain instance _Arrays_ di beberapa tempat (tentu akan memerlukan inisialisasi memory) hal ini juga menyebabkan adanya _redudance_ code yang cukup signifikan pada level controller class. Untuk menjawab pertanyaan tersebut, maka dibutuhkan suatu layer tambahan untuk menjadi penghubung antara logic pada controller yang menghasilkan kumpulan data product dengan penyedia data product tersebut.

Untuk berkenalan dengan layer `service`. Langkah pertama adalah dengan menambah Class `ProductService` pada package package `co.id.balicamp.javatrainee.service`

Beberapa point penting saat menambahkan service pada aplikasi web berbasis spring boot, diantaranya:
* Penambahan berupa Java Class
* Dilengkapi dengan annotations `@Service`
* Menyediakan method layanan untuk memproses pencarian data, maupun CRUD ke layer DAO dan dapat diinject pada layer controller, maupun sesama layer service

Langkah pertama adalah memindahkan init data product dari controller method `getAllProducts()` ke class service yang baru misalnya `ProductService` dan menyediakan method layanan untuk memproses pencarian kumpulan data product. 
```java
    package co.id.balicamp.javatrainee.service;

    import co.id.balicamp.javatrainee.model.Product;
    import org.springframework.stereotype.Service;

    import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;

    @Service
    public class ProductService {

        private List<Product> products = new ArrayList(Arrays.asList(new Product("P001", "Product 001", "Product Description 001"),
                new Product("P002", "Product 002", "Product Description 002"),
                new Product("P003", "Product 003", "Product Description 003"),
                new Product("P004", "Product 004", "Product Description 004")
        ));

        public List<Product> getAllProducts(){
            return products;
        }
    }
```

Annotation `@Service` menyatakan bahwa ketika Spring Boot dijalankan, maka akan melakukan scan package dan mengenali class dengan annotation tersebut merupakan Service class. Berikut adalah potongan code Class ProductService pada package `co.id.balicamp.javatrainee.service`. Berikutnya pada class controller saatnya menggunakan "_keajaiban_" spring dengan inject class `ProductService` dan akses method `getAllProducts` pada controller class. Sehingga butuh modifikasi class `ProductController` menjadi:
```java
// potongan code yang lainnya tidak ditampilkan agar tidak penuh
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    public List<Product> getAllProducts(){
        return productService.getAllProducts();
    }
}
```

**Bagaimana dengan layer DAO (_Data Access Object_) ?**

Dalam prakteknya struktur DAO digunakan sebagai pattern untuk mengisolasi bisnis layer dengan _persistence layer_ contohnya saat melakukan akses data maupun CRUD ke data storage/database. Untuk memahami bagaimana pemisahan struktur DAO dan service, pada artikel berikut diumpamakan kumpulan data product adalah resource hasil output dari database. Sehingga perlu penyesuaian dan penambahan package serta class. Beberapa point penting saat menambahkan dao pada aplikasi web berbasis spring boot, diantaranya:
* Penambahan DAO API berupa Java Class 
* Dilengkapi dengan annotations `@Repository`
* Menyediakan method layanan untuk memproses pencarian data, maupun CRUD ke level persitance (database) dan dapat diinject pada layer service

Penambahan class interface `ProductDao` sebagai DAO API pada package `co.id.balicamp.javatrainee.dao`
```java
package co.id.balicamp.javatrainee.dao;

import co.id.balicamp.javatrainee.model.Product;

import java.util.List;

public interface ProductDao {
    List<Product> getAllProducts();
}
```

Implementasi class interface `ProductDao` dan pemindahan parameter `List<Product>` sebagai kumpulan data static (perumpamaan hasil manipulasi data ke/dari database) pada package `co.id.balicamp.javatrainee.dao.impl`
```java
package co.id.balicamp.javatrainee.dao.impl;

import co.id.balicamp.javatrainee.dao.ProductDao;
import co.id.balicamp.javatrainee.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {

    private List<Product> products = new ArrayList(Arrays.asList(
            new Product("P001", "Product 001", "Product Description 001"),
            new Product("P002", "Product 002", "Product Description 002"),
            new Product("P003", "Product 003", "Product Description 003"),
            new Product("P004", "Product 004", "Product Description 004")
    ));

    @Override public List<Product> getAllProducts(){
        return products;
    }
}
```

Modifikasi class `ProductService` dengan melakukan inject dan menggunakan method `getAllProducts()` dari class `ProductDao`
```java
import co.id.balicamp.javatrainee.dao.ProductDao;

@Service
public class ProductService {

@Autowired
private ProductDao productDao;

public List<Product> getAllProducts(){
    return productDao.getAllProducts();
}
```

Pada prakteknya, penggunaan interface class serta implementasi dapat dilakukan sesuai dengan pendekatan masing-masing sistem analyst untuk mengakomudasi development environment pada suatu aplikasi enterprise. Layer service juga dapat diextract class implementasinya seperti struktur layer DAO. 

Tujuannya adalah ketika bekerja dalam skala development sebuah aplikasi enterprise yang besar, akan memungkinkan suatu DAO dan suatu service di akses dibanyak tempat, sehingga jika terjadi perubahan logic, maupun error handling maka cukup disesuaikan pada implementasi class saja sementara method interface tetap. Sehingga mengurangi effort refactor yang banyak. Struktur project dengan struktur MVC diatas kurang lebih akan menjadi seperti gambar berikut:

![maven-project_1](img/struktur_project.png)

**Lengkapi proses CRUD product**

Setelah berhasil menampilkan kumpulan data product, selanjutnya kita dapat lengkapi proses CRUD dengan mapping url path berikut

* `/products/id` untuk membaca data product berdasarkan id
    
    Tambahkan method pada class `ProductController` dengan mapping url path sebagai berikut:
    
    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @RequestMapping("/products/{idProduct}")
    public Product getProduct(@PathVariable("idProduct") String id){
        return productService.getProduct(id);
    }
    ```

    Pada potongan code diatas, digunakan annotation `@PathVariable` untuk memberi instruksi method pada controller class, url http yang diakses adalah `http://localhost:8080/products/XXX` dan memberikan logic pada method `getProduct` dengan input parameter `id` yang diambil nilainya dari `XXX` berdasarkan url path nya sebagai variable. Pada class `ProductService` ditambahkan:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    public Product getProduct(String id) {
        return productDao.getProductById(id);
    }
    ```

    Penambahan API pada class interface `ProductDao` 

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    Product getProductById(String id);
    ```

    Implementasi class `ProductDaoImpl`
    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @Override
    public Product getProductById(String id) {
        return products.stream().filter(p -> p.getId().equals(id)).findFirst().get();
    }
    ```

* `/products` untuk menambah data product
    
    Tambahkan method pada class `ProductController` dengan mapping url sebagai berikut:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @RequestMapping(method = RequestMethod.POST, value = "/products")
    public void addProduct(@RequestBody Product product){
        productService.addProduct(product);
    }
    ```

    Pada potongan code diatas, `@RequestMapping` dilengkapi dengan parameter `method = RequestMethod.POST` dan `value = "/products"` menyatakan bahwa url http yang diakses adalah `http://localhost:8080/products` dengan method `POST`. Annotation `@RequestBody` menyatakan method controller `addProduct` menerima input parameter Object `Product` sebagai variable dari http request body. Pada class `ProductService` ditambahkan:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    public void addProduct(Product product) {
        productDao.addProduct(product);
    }
    ```

    Penambahan API pada class interface `ProductDao`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    void addProduct(Product product);
    ```

    Implementasi class `ProductDaoImpl`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @Override
    public void addProduct(Product product) {
        products.add(product);
    }
    ```

* `/products/id` untuk melakukan update data product berdasarkan id
    
    Tambahkan method pada class `ProductController` dengan mapping url sebagai berikut:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @RequestMapping(method = RequestMethod.PUT, value = "/products/{id}")
    public void updateProduct(@RequestBody Product product, @PathVariable String id){
        productService.updateProduct(product, id);
    }
    ``` 

    Pada potongan code diatas, `@RequestMapping` dilengkapi dengan parameter ` method = RequestMethod.PUT` dan `value = "/products/{id}"` menyatakan bahwa url http yang diakses adalah `http://localhost:8080/products/XXX` dengan method `PUT`. Annotation `@RequestBody` menyatakan method controller `updateProduct` menerima input parameter Object `Product` sebagai variable dari http request body dan `@PathVariable` sebagai parameter `id` dengan nilai diambil dari url path `XXX`. Pada class `ProductService` ditambahkan:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    public void updateProduct(Product product, String id) {
        productDao.updateProduct(product, id);
    }
    ```

    Penambahan API pada class interface `ProductDao`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    void updateProduct(Product product, String id);
    ```

    Implementasi class `ProductDaoImpl`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @Override
    public void updateProduct(Product product, String id) {
        for (int i = 0; i < products.size(); i++) {
            Product p = products.get(i);
            if(p.getId().equals(id)){
                products.set(i, product);
                return;
            }
        }
    }
    ```

* `/products/id` untuk melakukan delete data product berdasarkan id
    
    Tambahkan method pada class `ProductController` dengan mapping url sebagai berikut:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @RequestMapping(method = RequestMethod.DELETE, value = "/products/{id}")
    public void deleteProduct(@PathVariable("id") String id){
        productService.deleteProduct(id);
    }
    ``` 

    Pada potongan code diatas, `@RequestMapping` dilengkapi dengan parameter `method = RequestMethod.DELETE` dan `value = "/products/{id}"` menyatakan bahwa url http yang diakses adalah `http://localhost:8080/products/XXX` dengan method `DELETE`. Annotation `@PathVariable` sebagai parameter `id` dengan nilai diambil dari url path `XXX`. Pada class `ProductService` ditambahkan:

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    public void deleteProduct(String id) {
        productDao.deleteProductById(id);
    }
    ```

    Penambahan API pada class interface `ProductDao`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    void deleteProductById(String id);
    ```

    Implementasi class `ProductDaoImpl`

    ```java
    // potongan code lainnya tidak ditampilkan agar tidak penuh
    @Override
    public void deleteProductById(String id) {
        products.removeIf(p -> p.getId().equals(id));
    }
    ```

**Testing dengan REST Client**

Untuk melakukan testing terhadap web service dan penambahan operasi CRUD sebelumnya, dapat digunakan aplikasi/tools pihak lain sebagai client untuk requestnya. Ada beberapa tools free yang dapat dipergunakan dan bertebaran diantaranya :

- [Postman](#https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en#) merupakan extension google chrome
- [SoapUI](#https://www.soapui.org/#) merupakan open-source web service testing yang dikembangkan oleh SmartBear
- [Boomerang](#https://chrome.google.com/webstore/detail/boomerang-soap-rest-clien/eipdnjedkpcnlmmdfdkgfpljanehloah?hl=en#) merupakan extension google chrome
- [Insomnia](#https://insomnia.rest/#) merupakan open-source tools yang dikembangkan oleh Floating Keyboard Software (fav. penulis 😅)
- dll


**Bekerja dengan file konfigurasi**

Pada package src/main/java/resource memungkinkan kita untuk mengatur berbagai macam konfigurasi tambahan yang dapat dimapping pada file `application.properties`.

Referensi lengkap untuk konfigurasi yang dapat dimapping pada `application.properties` dapat ditemukan pada [docs.spring.io](#https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html#)

Tambahkan beberapa properties spring

```java
server.port=101010 (sesuai keperluan)
spring.jackson.serialization.indent-output=true
```

#### spring initializr ####
Selain melalui create new maven project, langkah yang dapat ditempuh dalam membuat sebuah aplikasi berbasis spring boot adalah dengan menggunakan `spring initializr`

1. Browse ke https://start.spring.io/

2. Lengkapi Project Metadata

    Group
    
    ```java
    co.id.balicamp.javatrainee
    ```

    Artifact
    
    ```
    spring-boot-demo
    ```
   
    Dependencies
    
    ```java
    Web
    ```
   
3. Generate Project

4. Download dan import as maven project pada IDE masing-masing 

5. Build dan Run via Maven 

    ```java
    mvn clean spring-boot:run
    ```
    
6. Coba akses via browser local ke halaman `http://localhost:8080/`  


### Serba Serbi ###

- Konvension name controller
- Konvension url path REST controller
- Konvension name dao class
- Konvension name method dao
- Konvension name service class
- Konvension name service method
- Konvension name model/dto

### Whats Next? ###

* Hibernate ORM
* Spring Data JPA
* Spring Transaction

## Documentation ##
* https://spring.io
* https://docs.spring.io
* https://projectlombok.org
* [image] https://i2.wp.com/aryadharmaadi.com/blog/wp-content/uploads/2016/05/MVC.png
* https://www.theserverside.com/news/1364527/Introduction-to-the-Spring-Framework